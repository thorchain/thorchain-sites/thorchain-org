import { AppProps } from 'next/app'
import Head from 'next/head'

import '../styles/bootstrap.css'
import '../styles/thorchain.css'
import '../styles/helper.css'

import Layout from 'components/Layout'

export default function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="THORChain" />
        <meta property="og:image" content="/images/preview-thorchain.png" />
        <meta property="og:description" content="THORChain - a decentralised liquidity network" />
        <meta property="og:url" content="https://thorchain.org" />
        <link rel="icon" href="/images/favicon.png" />
        <meta name="twitter:card" content="/images/preview-thorchain.png" />
        <meta name="keywords" content="Binance, Binance Chain, Liquidity, THORChain, Swap Assets" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no"
        />
        <link
          href="https://fonts.googleapis.com/css?family=Exo+2:300,400|Open+Sans:200,300,400|Titan+One"
          rel="stylesheet"
        />
      </Head>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </>
  )
}
