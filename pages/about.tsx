import RoadmapSection from 'components/Home/RoadmapSection'
import GovernanceSection from 'components/Home/GovernanceSection'
import TeamSection from 'components/Home/TeamSection'

const Home = () => (
  <>
    <RoadmapSection />
    <GovernanceSection />
    <TeamSection />
  </>
)

export default Home
