import Head from 'next/head'

import HeroSection from 'components/Home/HeroSection'
import FeaturesSection from 'components/Home/FeaturesSection'
import RuneSection from 'components/Home/RuneSection'
import InfoSection from 'components/Home/InfoSection'

const Home = () => (
  <>
    <Head>
      <title>THORChain</title>
    </Head>
    <HeroSection />
    <FeaturesSection />
    <RuneSection />
    <InfoSection />
  </>
)

export default Home
