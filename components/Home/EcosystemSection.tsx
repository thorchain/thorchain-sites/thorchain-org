import React, { FC } from 'react'

export const EcosystemSection: FC = () => (
  <section id="Ecosystem">
    <div className="container-fluid lgrey-bg min-h-30v pt-20vh">
      <div className="container ecosystem-container">
        <div className="row">
          <div className="col-md-12 col-lg-6">
            <h1>ECOSYSTEM</h1>
            <h5>Join and explore THORChain's vibrant ecosystem of community-run initiatives and more.</h5>
            <br />
            {/* <p className="desc-paragraph">
              THORChain is supported by many individuals and teams working for
              decentralised and democratised liquidity.
                <br />
              <br />
            </p> */}
          </div>
          <div className="col-md-12 col-lg-6">
            {/* <div className="container  text-center" style={{ justifyContent: 'center' }}>
              <a href="https://runevault.org" target="_blank">
                <div className="thorchain-component" style={{ display: 'inline-block' }}>
                  <h4>
                  EARN LIQUIDITY REWARDS
                </h4>
                  <div
                    className="bg-thorchain-gradient-center w-100p"
                    style={{ height: '1px', marginBottom: '10px' }}
                  />
                  <p>Earn a weekly reward (~30%) to participate in RUNEVault</p>
                  <p style={{ color: '#00ccff' }}>runevault.org/stake</p>
                </div>
              </a>
              <a href="https://runevault.org" target="_blank">
                <div className="thorchain-component" style={{ display: 'inline-block' }}>
                  <h4>
                  WIN A COLLECTIBLE
                </h4>
                  <div
                    className="bg-thorchain-gradient-center w-100p"
                    style={{ height: '1px', marginBottom: '10px' }}
                  />
                  <p>Win a unique THORChain collectible by participating in staking.</p>
                  <p style={{ color: '#00ccff' }}>runevault.org/collectibles</p>
                </div>
              </a>
            </div> */}
          </div>
        </div>
      </div>
    </div>
    <div className="container-fluid grey-bg min-h-60v pt-10vh">
      <div className="container ecosystem-container">
        <div className="row component-row my-30 text-left">
          <div className="row">
            <div className="col-12">
              <h2>COMMUNITY</h2>
              <br />
              <p className="desc-paragraph">
                The community run several initiatives to promote THORChain.
                <br />
                <br />
              </p>
            </div>
          </div>
          <div className="container  text-center" style={{ justifyContent: 'center' }}>
            <a href="https://thorchain.community/" target="_blank">
              <div className="thorchain-component" style={{ display: 'inline-block' }}>
                <h4>
                  RUNE CALCULATOR
                </h4>
                <div
                  className="bg-thorchain-gradient-center w-100p"
                  style={{ height: '1px', marginBottom: '10px' }}
                />
                <p>Explore RUNE's unique deterministic pricing relationship.</p>
                <p style={{ color: '#00ccff' }}>thorchain.community</p>
              </div>
            </a>
            
            
            
          </div>

        </div>
        <div className="row">
          <div className="col-6">
            <br /><br />
            <p> <a href="https://t.me/thorchain_org" target="_blank" style={{ color: '#50E3C2' }}>@Thorchain_org - Thorchain Community EN (Official, English)</a></p>
            <p> <a href="https://t.me/thorchain" target="_blank" style={{ color: '#50E3C2' }}>@Thorchain - Thorchain Announcements EN (Official, English)</a></p>
            <p> <a href="https://t.me/thorchain_dev" target="_blank" style={{ color: '#50E3C2' }}>@thorchain_dev - Thorchain Devs EN (Official, English)</a></p>
            <p> <a href="https://t.me/RuneTrading" target="_blank" style={{ color: '#50E3C2' }}>@RuneTrading - Thorchain Trading EN (Not official, English)</a></p>
            <p> <a href="https://t.me/RuneTrollBox " target="_blank" style={{ color: '#50E3C2' }}>@RuneTrollBox - Thorchain Trollbox EN (Not official, English)</a></p>
            <p> <a href="https://t.me/thorchain_memes" target="_blank" style={{ color: '#50E3C2' }}>@thorchain_memes - Thorchain Memes (Not official)</a></p>
          </div>
          <div className="col-6">
            <br /><br />
            <p> <a href="https://t.me/THORChainChinese" target="_blank" style={{ color: '#50E3C2' }}>@THORChainChinese - Thorchain Ann CN (Not official, Chinese)</a></p>
            <p> <a href="https://t.me/ThorchainRussian" target="_blank" style={{ color: '#50E3C2' }}>@ThorchainRussian - Thorchain Community RU (Not official, Russian)</a></p>
            <p> <a href="https://t.me/ThorchainRU" target="_blank" style={{ color: '#50E3C2' }}>@ThorchainRU - Thorchain Announcements RU (Not official, Russian)</a></p>
            <p> <a href="https://t.me/doza_hopiuma" target="_blank" style={{ color: '#50E3C2' }}>Thorchain hopium RU (Not official, Russian)</a></p>
          </div>
        </div>
      </div>
    </div>
    <div className="container-fluid lgrey-bg min-h-60v pt-10vh">
      <div className="container ecosystem-container">
        <div className="row component-row my-30 text-left">

          <div className="row">
            <div className="col-12">
              <h2>TOOLS</h2>
              <br />
              <p className="desc-paragraph">
                Learn, view and explore THORChain using these community-built tools.
                <br />
                <br />
              </p>
            </div>
          </div>
          <div className="container  text-center" style={{ justifyContent: 'center' }}>
            <a href="https://runestake.info/" target="_blank">
              <div className="thorchain-component" style={{ display: 'inline-block' }}>
                <h4>
                  <span className="pr-10px"></span>RUNESTAKE.INFO
                </h4>
                <div
                  className="bg-thorchain-gradient-center w-100p"
                  style={{ height: '1px', marginBottom: '10px' }}
                />
                <p>View returns on your liquidity.</p>
                <p style={{ color: '#00ccff' }}>runestake.info</p>
              </div>
            </a>
            <a
              href="https://runedata.info"
              target="_blank"
            >
              <div className="thorchain-component" style={{ display: 'inline-block' }}>
                <h4>
                  <span className="pr-10px"></span>
                  RUNEDATA.INFO
                </h4>
                <div
                  className="bg-thorchain-gradient-center w-100p"
                  style={{ height: '1px', marginBottom: '10px' }}
                />
                <p>
                  View and simulate returns for Liquidity Pools
                </p>
                <p style={{ color: '#00ccff' }}>runedata.info</p>
              </div>
            </a>
            <a href="https://runebalance.com" target="_blank">
              <div className="thorchain-component" style={{ display: 'inline-block' }}>
                <h4>
                  <span className="pr-10px"></span> RUNEBALANCE.COM
                </h4>
                <div
                  className="bg-thorchain-gradient-center w-100p"
                  style={{ height: '1px', marginBottom: '10px' }}
                />
                <p>
                  View arbitrage opportunities between RUNE pools. 
                </p>
                <p style={{ color: '#00ccff' }}>runebalance.com</p>
              </div>
            </a>
            <a href="https://leaderboard.thornode.org" target="_blank">
              <div className="thorchain-component" style={{ display: 'inline-block' }}>
                <h4>
                  <span className="pr-10px"></span> TRADER LEADERBOARD
                </h4>
                <div
                  className="bg-thorchain-gradient-center w-100p"
                  style={{ height: '1px', marginBottom: '10px' }}
                />
                <p>
                  View the top traders on THORChain.
                </p>
                <p style={{ color: '#00ccff' }}>leaderboard.thornode.org</p>
              </div>
            </a>
            <a href="https://t.me/thorchain_alert" target="_blank">
              <div className="thorchain-component" style={{ display: 'inline-block' }}>
                <h4>
                  <span className="pr-10px"></span> THORChain Telegram Bot
                </h4>
                <div
                  className="bg-thorchain-gradient-center w-100p"
                  style={{ height: '1px', marginBottom: '10px' }}
                />
                <p>
                  Telegram bot to view key THORChain network changes.
                </p>
                <p style={{ color: '#00ccff' }}>@thorchain_alert</p>
              </div>
            </a>
            <a href="https://twitter.com/thor_bot" target="_blank">
              <div className="thorchain-component" style={{ display: 'inline-block' }}>
                <h4>
                  <span className="pr-10px"></span> THORChain Twitter Bot
                </h4>
                <div
                  className="bg-thorchain-gradient-center w-100p"
                  style={{ height: '1px', marginBottom: '10px' }}
                />
                <p>
                  Twitter bot to view key THORChain network changes.
                </p>
                <p style={{ color: '#00ccff' }}>@thor_bot</p>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div className="container-fluid grey-bg min-h-60v pt-10vh pb-10vh">
      <div className="container ecosystem-container">
        <div className="row component-row my-30 text-left">

          <div className="row">
            <div className="col-12">
              <h2>EDUCATION</h2>
              <br />
              <p className="desc-paragraph">
                Learn how THORChain works.
                <br />
                <br />
              </p>
            </div>
          </div>
          <div className="container  text-center" style={{ justifyContent: 'center' }}>

            <a href="https://docs.thorchain.org/" target="_blank">
              <div className="thorchain-component" style={{ display: 'inline-block' }}>
                <h4>
                  OFFICIAL DOCS
                </h4>
                <div
                  className="bg-thorchain-gradient-center w-100p"
                  style={{ height: '1px', marginBottom: '10px' }}
                />
                <p>
                  The official THORChain Documentation.
                </p>
                <p style={{ color: '#00ccff' }}>
                  docs.thorchain.org
                </p>
              </div>
            </a>
            <a href="https://rebase.foundation/network/thorchain/" target="_blank">
              <div className="thorchain-component" style={{ display: 'inline-block' }}>
                <h4>
                  REBASE FOUNDATION
                </h4>
                <div
                  className="bg-thorchain-gradient-center w-100p"
                  style={{ height: '1px', marginBottom: '10px' }}
                />
                <p>
                  An easy-to-understand overview of THORChain.
                </p>
                <p style={{ color: '#00ccff' }}>
                rebase.foundation/network/thorchain/
                </p>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div className="container-fluid lgrey-bg min-h-60v pt-10vh">
      <div className="container ecosystem-container">
        <div className="row component-row my-30 text-left">

          <div className="row">
            <div className="col-12">
              <h2>EXPLORERS</h2>
              <br />
              <p className="desc-paragraph">
                Explore THORChain using these block explorers.
                <br />
                <br />
              </p>
            </div>
          </div>
          <div className="container  text-center" style={{ justifyContent: 'center' }}>
            <a href="https://thorchain.net/" target="_blank">
              <div className="thorchain-component" style={{ display: 'inline-block' }}>
                <h4>
                  <span className="pr-10px"></span>THORCHAIN.NET
                </h4>
                <div
                  className="bg-thorchain-gradient-center w-100p"
                  style={{ height: '1px', marginBottom: '10px' }}
                />
                <p>A lightweight, open-source THORChain explorer.</p>
                <p style={{ color: '#00ccff' }}>thorchain.net</p>
              </div>
            </a>
            <a
              href="https://viewblock.io/thorchain"
              target="_blank"
            >
              <div className="thorchain-component" style={{ display: 'inline-block' }}>
                <h4>
                  <span className="pr-10px"></span>
                  THORCHAIN Block Explorer
                </h4>
                <div
                  className="bg-thorchain-gradient-center w-100p"
                  style={{ height: '1px', marginBottom: '10px' }}
                />
                <p>
                  Official Block Explorer built by ViewBlock.
                </p>
                <p style={{ color: '#00ccff' }}>viewblock.io/thorchain</p>
              </div>
            </a>
            <a href="https://defi.delphidigital.io/thorchain" target="_blank">
              <div className="thorchain-component" style={{ display: 'inline-block' }}>
                <h4>
                  <span className="pr-10px"></span> Delphi Digital Dashboard
                </h4>
                <div
                  className="bg-thorchain-gradient-center w-100p"
                  style={{ height: '1px', marginBottom: '10px' }}
                />
                <p>
                  A dashboard built for THORChain by the Delphi Digital Team
                </p>
                <p style={{ color: '#00ccff' }}>defi.delphidigital.io/thorchain</p>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
    
  </section >
)

export default EcosystemSection
