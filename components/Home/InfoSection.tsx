import React, { FC } from 'react'
import Link from 'next/link'

export const InfoSection: FC = () => (
    <div className="container-fluid grey-bg min-h-60v pb-10vh" id="INFO">
        <div className="container">
            <div className="row text-center  pt-10vh">
                <div className="col-12 pb-50">
                    <h2>PROJECT</h2><br />

                    <div className="row">
                        <div className="col-lg-6 col-md-12">
                            <Link href="/explore">
                                <a>
                                    <div className="thorchain-component" style={{ display: 'inline-block' }}>
                                        <h4>EXPLORE</h4>
                                        <div
                                            className="bg-thorchain-gradient-center w-100p"
                                            style={{ height: '1px', marginBottom: '10px' }}
                                        />
                                        <p>Explore the THORChain ecosystem, including wallets, block explorers, dashboards and more.</p>
                                    </div>
                                </a>
                            </Link>
                        </div>
                        <div className="col-lg-6 col-md-12">
                        <Link href="/dev">
                                <a>
                                <div className="thorchain-component" style={{ display: 'inline-block' }}>
                                    <h4>DEVELOPERS</h4>
                                    <div
                                        className="bg-thorchain-gradient-center w-100p"
                                        style={{ height: '1px', marginBottom: '10px' }}
                                    />
                                    <p>Read the documentation, review the audits, learn how to connect and build on THORChain.</p>
                                </div>
                                </a>
                            </Link>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-lg-6 col-md-12">
                        <Link href="/tech">
                                <a>
                                <div className="thorchain-component" style={{ display: 'inline-block' }}>
                                    <h4>TECHNOLOGY</h4>
                                    <div
                                        className="bg-thorchain-gradient-center w-100p"
                                        style={{ height: '1px', marginBottom: '10px' }}
                                    />
                                    <p>Learn how THORChain works in detail, including how it solves liquidity, security and scalability.</p>
                                </div>
                                </a>
                            </Link>
                        </div>
                        <div className="col-lg-6 col-md-12">
                        <Link href="/about">
                                <a>
                                <div className="thorchain-component" style={{ display: 'inline-block' }}>
                                    <h4>ABOUT</h4>
                                    <div
                                        className="bg-thorchain-gradient-center w-100p"
                                        style={{ height: '1px', marginBottom: '10px' }}
                                    />
                                    <p>Learn about the roadmap, team, community and project information.</p>
                                </div>
                                </a>
                            </Link>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>
)

export default InfoSection