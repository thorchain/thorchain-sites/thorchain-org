import React, { FC } from 'react'

export const FeaturesSection: FC = () => (
  <>
    <div className="container-fluid grey-bg min-h-60v pt-10vh pb-10vh">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 col-md-12">
            <div className="col-12 pitch-text">
              <h2 className="my-5">SWAP ASSETS</h2>
              <h5> </h5>
              <ul>
                <li>Swap between any connected asset</li>
                <li>Pay a transparent fee for access to liquidity</li>
                <li>Swaps are non-custodial (no third-parties)</li>
                <li>Swap directly from supported wallets</li>
              </ul>
            </div>
            <div className="row my-5">
              <div className="col-12 py-1">
                <h5>
                  <a href="https://docs.thorchain.org/roles/swapping" target="_blank" style={{ color: '#50E3C2' }}>
                    LEARN MORE
                  </a>
                </h5>
              </div>
            </div>
          </div>
          <div className="col-lg-6 min-h-40v thorchain-animated-container">
            <div className="col-12">
              <div className="thorchain-img">
                <img
                  className="thorchain-swirl centered spin-anim-anti-clockwise"
                  src={require('assets/img/animated/clockwise.svg')}
                />
                <img
                  className="thorchain-swirl centered spin-anim-clockwise"
                  src={require('assets/img/animated/anti-clockwise.svg')}
                />
                <img
                  className="centered thorchain-swirl-centerpiece"
                  src={require('assets/img/rune-icon.svg')}
                />
                <img
                  className="img-fluid centered liquidity-circle"
                  src={require('assets/img/liquidity-circle.png')}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="container-fluid min-h-60v pt-10vh pb-10vh" id="STAKE">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 text-center" />
          <div className="col-lg-6 col-md-12">
            <h2 className="my-1">ADD LIQUIDITY</h2>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 text-center">
            <div className="container">
              <div className="col">
                <img className="img-fluid" src={require('assets/img/liquidity-pools.png')} />
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-md-12">
            <div className="container pitch-text py-5">
              <h5> </h5>
              <ul>
                <li>Add liquidity in existing pools, or create new ones</li>
                <li>Liquidity providers earn fees and block rewards</li>
                <li>Liquidity is always on-chain and never “pegged” or “wrapped”</li>
                <li>Pooled capital is always underwritten by the system</li>
              </ul>
            </div>
            <div className="row my-5">
              <div className="col-12 py-1 mx-1">
                <h5>
                  <a
                    href="https://docs.thorchain.org/roles/staking"
                    target="_blank"
                    style={{ color: '#50E3C2' }}
                  >
                    LEARN MORE
                  </a>
                </h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="container-fluid grey-bg min-h-60v pt-10vh pb-10vh" id="NODE">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 col-md-12">
            <div className="col-12 pitch-text">
              <h2 className="my-5">BOND AS A NODE</h2>
              <h5> </h5>
              <ul>
              <li>Nodes compete to enter with bonded capital</li>
              <li>Nodes earn 2/3rds of the System Income</li>
                <li>
                  The system churns nodes every 3 days, ensuring liveness and preventing capture
                </li>
                <li>Nodes are anonymous and cannot corrupt the system</li>
              </ul>
            </div>
            <div className="row my-5">
              <div className="col-12 py-1 mx-1">
                <h5>
                  <a
                    href="https://docs.thorchain.org/thornodes/overview"
                    target="_blank"
                    style={{ color: '#50E3C2' }}
                  >
                    LEARN MORE
                  </a>
                </h5>
              </div>
            </div>
          </div>
          <div className="col-lg-6 text-center my-5">
            <div className="container">
              <div className="row my-5">
                <div className="col mx-3 py-5">
                  <img className="img-fluid" src={require('assets/img/node-network.png')} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </>
)

export default FeaturesSection
