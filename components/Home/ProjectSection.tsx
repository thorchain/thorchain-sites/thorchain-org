import React, { FC } from 'react'

export const ProjectSection: FC = () => (
  <div className="container-fluid grey-bg min-h-60v pb-10vh" id="PROJECT">
    <div className="container">
      <div className="row text-center  pt-10vh">
        <div className="col-12 pb-50">
          <h2 style={{ letterSpacing: '2.75px' }}>PROJECT</h2>
          <div
            className="bg-thorchain-gradient-center"
            style={{ margin: '20px auto', height: '2px', width: '75%' }}
          />
          <h5>OTHER DETAILS ABOUT THORCHAIN</h5>
        </div>
      </div>
      <div className="row pt-5vh">
        <div className="col-lg-6 col-md-1">
          <div className="container pitch-text">
            {/* <h5>IMPORTANT ASPECTS</h5> */}
            <ul>
              <li>The project was seed-funded in 2018</li>
              <li>RUNE was listed on Binance DEX to assist distribution prior to mainnet</li>
              <li>Weekly development updates cover project progress</li>
              <li>Monthly treasury reports cover project finance</li>
            </ul>
          </div>
        </div>
        <div className="col-lg-6 col-md-1">
          <div className="container pitch-text">
            <div className="row py-3">
              <div className="col-12 py-1 mx-1 text-center">
                <h6>
                  <a
                    href="https://medium.com/thorchain"
                    target="_blank"
                    style={{ color: '#50E3C2' }}
                  >
                    PROJECT BLOG
                  </a>
                </h6>
              </div>
              <div className="col-12 py-1 mx-1 text-center">
                <h6>
                  <a
                    href="https://community.binance.org/topic/357/proposal-to-list-thorchain-rune-on-binance-dex"
                    target="_blank"
                    style={{ color: '#50E3C2' }}
                  >
                    BINANCEDEX LISTING ANN
                  </a>
                </h6>
              </div>
              
              <div className="col-12 py-1 mx-1 text-center">
                <h6>
                  <a href="https://docs.thorchain.org" target="_blank" style={{ color: '#50E3C2' }}>
                    DOCUMENTATION
                  </a>
                </h6>
              </div>
              <div className="col-12 py-1 mx-1 text-center">
                <h6>
                  <a href="https://bepswap.com" target="_blank" style={{ color: '#50E3C2' }}>
                    BEPSWAP PAGE
                  </a>
                </h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default ProjectSection
