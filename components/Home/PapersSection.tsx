import React, { FC } from 'react'

export const PapersSection: FC = () => (
  <section>
    <div className="container  pt-20vh ">
      <div className="row text-center" style={{ marginBottom: '50px' }}>
        <div className="col-12">
          <h1>THORCHAIN PAPERS</h1>
          <h5>Whitepapers, Audits and more.</h5>
        </div>
      </div>

      <div className="row" style={{ marginBottom: '50px' }}>
        <div className="col-lg-3 col-md-12">
          <a
            href="https://github.com/thorchain/Resources/blob/master/Whitepapers/THORChain-Whitepaper-May2020.pdf"
            target="_blank"
          >
            <div className="card">
              <img
                src={require('assets/img/papers/thorchain-whitepaper.png')}
                style={{ height: '297px' }}
              />
            </div>
          </a>
          <h4 style={{ textAlign: 'center' }}>THORChain White Paper</h4>
          <p style={{ textAlign: 'center' }}>MAY 2020</p>
        </div>
        <div className="col-lg-3 col-md-12">
          <a
            href="https://github.com/thorchain/Resources/blob/master/Whitepapers/THORChain-Cryptoeconomic-Paper-May2020.pdf"
            target="_blank"
          >
            <div className="card">
              <img
                src={require('assets/img/papers/economics-whitepaper.png')}
                style={{ height: '297px' }}
              />
            </div>
          </a>
          <h4 style={{ textAlign: 'center' }}>THORChain Crypto-Economic Paper</h4>
          <p style={{ textAlign: 'center' }}>MAY 2020</p>
        </div>
        <div className="col-lg-3 col-md-12">
          <a
            href="https://github.com/thorchain/Resources/blob/master/Whitepapers/THORChain-TSS-Paper-June2020.pdf"
            target="_blank"
          >
            <div className="card">
              <img
                src={require('assets/img/papers/thorchain-tss.png')}
                style={{ height: '297px' }}
              />
            </div>
          </a>
          <h4 style={{ textAlign: 'center' }}>THORChain TSS Paper</h4>
          <p style={{ textAlign: 'center' }}>JUNE 2020</p>
        </div>
        <div className="col-lg-3 col-md-12">
          <a
            href="https://github.com/thorchain/Resources/blob/master/Whitepapers/THORChain-TSS-Benchmark-July2020.pdf"
            target="_blank"
          >
            <div className="card">
              <img
                src={require('assets/img/papers/thorchain-tss-benchmark.png')}
                style={{ height: '297px' }}
              />
            </div>
          </a>
          <h4 style={{ textAlign: 'center' }}>TSS Benchmark Paper</h4>
          <p style={{ textAlign: 'center' }}>July 2020</p>
        </div>
      </div>
      <div className="row" style={{ marginBottom: '50px' }}>
        <div className="col-lg-4 col-md-12">
          <a
            href="https://github.com/thorchain/Resources/blob/master/Audits/THORChain-Certik-CodeReview-Mar2020.pdf"
            target="_blank"
          >
            <div className="card">
              <img
                src={require('assets/img/papers/certik-audit.png')}
                style={{ height: '297px' }}
              />
            </div>
          </a>
          <h4 style={{ textAlign: 'center' }}>Certik Code Review</h4>
          <p style={{ textAlign: 'center' }}>APRIL 2020</p>
        </div>
        <div className="col-lg-4 col-md-12">
          <a
            href="https://github.com/thorchain/Resources/blob/master/Audits/THORChain-Kudelski-TSS-Audit-June2020.pdf"
            target="_blank"
          >
            <div className="card">
              <img
                src={require('assets/img/papers/kudelski-audit.png')}
                style={{ height: '297px' }}
              />
            </div>
          </a>
          <h4 style={{ textAlign: 'center' }}>Kudelski TSS Audit</h4>
          <p style={{ textAlign: 'center' }}>MAY 2020</p>
        </div>
        <div className="col-lg-4 col-md-12">
          <a
            href="https://github.com/thorchain/Resources/blob/master/Audits/THORChain-Gauntlet-EconomicSecurityReview-May2020.pdf"
            target="_blank"
          >
            <div className="card">
              <img
                src={require('assets/img/papers/gauntlet-audit.png')}
                style={{ height: '297px' }}
              />
            </div>
          </a>
          <h4 style={{ textAlign: 'center' }}>Gauntlet Economic Security Review</h4>
          <p style={{ textAlign: 'center' }}>JUNE 2020</p>
        </div>
      </div>
    </div>
  </section>
)

export default PapersSection
