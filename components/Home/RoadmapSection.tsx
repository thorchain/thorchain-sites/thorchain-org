import React, { FC } from 'react'

export const Roadmap: FC = () => (
  <div className="container-fluid grey-bg min-h-60v pt-10vh pb-10vh" id="ROADMAP">
    <div className="container">
      <div className="row text-center  pt-10vh">
        <div className="col-12 pb-50">
          <h2 style={{ letterSpacing: '2.75px' }}>ROADMAP</h2>
          <div
            className="bg-thorchain-gradient-center"
            style={{ margin: '20px auto', height: '2px', width: '75%' }}
          />
          <h5>THORCHAIN IS A MULTI-YEAR PUBLIC PROJECT</h5>
        </div>
      </div>
      <div className="row mt-10v mx-10">
        <div style={{ width: '100%' }}>
          <div
            className="thorchain-component-l roadmap-component pitch-text"
            style={{ margin: '0 auto' }}
          >
            <h4 style={{ marginTop: '5px' }}>PUBLIC ALPHA</h4>
            <div className="row">
              <div className="col-12">
                <div className="pill-component">
                  <h5 style={{ marginTop: '5px' }}>DELIVERED</h5>
                </div>
              </div>
              <div className="col-12">
                <h5>
                  <a href="https://bepswap.com" target="_blank" style={{ color: '#50E3C2' }}>
                    VIEW PRODUCT
                  </a>
                </h5>
              </div>
            </div>
            <div className="pitch-text">
              <p className="desc-paragraph">
                First testnet on Binance Chain allowing simple swaps. Non-public API. No public
                nodes and no active churning. No bonded capital.{' '}
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="row mt-5v mx-10">
        <div style={{ width: '100%' }}>
          <div
            className="thorchain-component-l roadmap-component pitch-text"
            style={{ margin: '0 auto' }}
          >
            <h4 style={{ marginTop: '5px' }}>TESTNET</h4>
            <div className="row">
              <div className="col-12">
                <div className="pill-component">
                  <h5 style={{ marginTop: '5px' }}>DELIVERED </h5>
                </div>
              </div>
              <div className="col-12">
                <h5>
                  <a
                    href="https://testnet-api.bepswap.net/v1/doc"
                    target="_blank"
                    style={{ color: '#50E3C2' }}
                  >
                    VIEW MIDGARD API
                  </a>
                </h5>
              </div>
            </div>
            <div className="pitch-text">
              <p className="desc-paragraph">
                Second testnet on Binance Chain allowing swaps, stakes, and withdrawals. Public
                Midgard API. No public nodes and no active churning. Bonded capital, but no block
                rewards.{' '}
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="row mt-5v mx-10">
        <div style={{ width: '100%' }}>
          <div
            className="thorchain-component-l roadmap-component pitch-text"
            style={{ margin: '0 auto' }}
          >
            <h4 style={{ marginTop: '5px' }}>CHAOSNET</h4>
            <div className="row">
              <div className="col-12">
                <div className="pill-component">
                  <h5 style={{ marginTop: '5px' }}>DELIVERED</h5>
                </div>
              </div>
              <div className="col-12">
                <h5>
                  <a
                    href="https://viewblock.io/thorchain"
                    target="_blank"
                    style={{ color: '#50E3C2' }}
                  >
                    VIEW NETWORK
                  </a>
                </h5>
              </div>
            </div>
            <div className="pitch-text">
              <p className="desc-paragraph">
                Launch on Binance Chain with swaps, stakes, and bonding. Public nodes participating,
                with active churn. Block rewards with liquidity incentives. System capital limited
                to 600k RUNE. ChaosNet ends in a Ragnarök - a global shutdown.{' '}
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="row mt-5v mx-10">
        <div style={{ width: '100%' }}>
          <div
            className="thorchain-component-l roadmap-component pitch-text"
            style={{ margin: '0 auto' }}
          >
            <h4 style={{ marginTop: '5px' }}>Multi-chain Chaosnet</h4>
            <div className="row">
              <div className="col-12">
                <div className="pill-component">
                  <h5 style={{ marginTop: '5px' }}>TESTNET</h5>
                </div>
                <div className="col-12">
                <h5>
                  <a
                    href="https://testnet.asgard.exchange/"
                    target="_blank"
                    style={{ color: '#50E3C2' }}
                  >
                    VIEW NETWORK
                  </a>
                </h5>
              </div>
              </div>
            </div>
            <div className="pitch-text">
              <p className="desc-paragraph">
                Binance Chain, Ethereum and Bitcoin. Public nodes participating, with active churn.
                Block rewards with liquidity incentives. Cap on staked amount. Public audit
                completed.{' '}
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="row mt-5v mx-10">
        <div style={{ width: '100%' }}>
          <div
            className="thorchain-component-l roadmap-component pitch-text"
            style={{ margin: '0 auto' }}
          >
            <h4 style={{ marginTop: '5px' }}>ASGARDEX - MAINNET</h4>
            <div className="row">
              <div className="col-12">
                <div className="pill-component">
                  <h5 style={{ marginTop: '5px' }}>IN SCOPE</h5>
                </div>
              </div>
            </div>
            <div className="pitch-text">
              <p className="desc-paragraph">Mainnet with no caps. Decentralised.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default Roadmap
